#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

// priority: + - vs. * / vs ( )

// e1 + e2 - e3 + e4

// e1, e2, e3, e4 is of a form: t1 * t2 / t3

// missing features: handling of whitespace characters, multi-digit immediate numbers, EOF testing, better error handling

int peekchar ( ) {
	return ungetc ( getchar ( ), stdin );
}

double expression ( );

double factor ( ) {
	if ( isdigit ( peekchar ( ) ) ) {
		return (double) ( getchar ( ) - '0' );
	} else if ( peekchar ( ) == '(' ) {
		getchar ( );
		double res = expression ( );
		if ( peekchar ( ) != ')' )
			exit ( 1 ); // not the best here but there is not enough time:-)
		getchar ( );
		return res;
	} else {
		exit ( 2 ); // not the best here but there is not enough time:-)
	}
}

double term ( ) {
	double res = factor ( );
	while ( peekchar ( ) == '*' || peekchar ( ) == '/' ) {
		if ( peekchar ( ) == '*' ) {
			getchar ( );
			res *= factor ( );
		} else {
			getchar ( );
			res /= factor ( );
		}
	}
	return res;
}

double expression ( ) {
	double res = term ( );
	while ( peekchar ( ) == '+' || peekchar ( ) == '-' ) {
		if ( peekchar ( ) == '+' ) {
			getchar ( );
			res += term ( );
		} else {
			getchar ( );
			res -= term ( );
		}
	}
	return res;
}

int main ( ) {
	double res = expression ( );
	printf ( "%f\n", res );
	return 0;
}
