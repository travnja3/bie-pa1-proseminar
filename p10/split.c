#include <stdio.h>
#include <stdlib.h>

unsigned g_count;

int canDivide3Inner ( int * values, int valuesNr, int maxPartitionValue, int x1, int x2, int x3 ) {
	g_count ++;
	if ( valuesNr == 0 )
		return x1 == x2 && x2 == x3;

	if ( maxPartitionValue < x1 || maxPartitionValue < x2 || maxPartitionValue < x3 )
		return 0;

	return canDivide3Inner ( values + 1, valuesNr - 1, maxPartitionValue, x2, x3, x1 + values[0] ) ||
		canDivide3Inner ( values + 1, valuesNr - 1, maxPartitionValue, x1, x3, x2 + values[0] ) ||
		canDivide3Inner ( values + 1, valuesNr - 1, maxPartitionValue, x1, x2, x3 + values[0] );
}

int ascCompare ( const void * a, const void * b ) {
	const int * ai = ( const int * ) a;
	const int * bi = ( const int * ) b;

	return ( * bi < * ai ) - ( * ai < * bi );
}

int canDivide3 ( int * values, int valuesNr ) {
	if ( valuesNr == 0 )
		return 1;

	int sum = 0;
	int max = values [ 0 ];
	for ( int i = 0; i < valuesNr; ++ i ) {
		sum += values [ i ];
		if ( max < values [ i ] )
			max = values [ i ];
	}

	if ( sum % 3 != 0 )
		return 0;

	if ( max > sum / 3 )
		return 0;

	qsort ( values, valuesNr, sizeof ( * values ), ascCompare );

	return canDivide3Inner ( values + 1, valuesNr - 1, sum / 3, 0, 0, values [0] );
}

int main ( ) {
	int items[] = { 6, 5, 4, 3, 3, 3, 3, 2, 2, 2 }; // 6 + 5, 4 + 3 + 2 + 2, 3 + 3 + 3 + 2
//	int items[] = { 1, 1, 1, 2, 2, 2, 3, 3, 3 };
//	int items[] = { 3, 3, 3, 2, 2, 2, 1, 1, 1 };
//	int items[] = { 1, 2, 3, 1, 2, 3, 1, 2, 3 };
	printf ( "%s\n", canDivide3 ( items, sizeof(items)/sizeof(items[0]) ) ? "yes" : "no" );
	printf ( "calls = %u\n", g_count );
	return 0;
}
