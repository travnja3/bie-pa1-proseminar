#include <stdio.h>
#include <stdlib.h>

#define ARRAY_MAX 100

unsigned g_count;

// let's assume max is never 0
int cache [ ARRAY_MAX ];

int maximum ( int * array, int len ) {
	++ g_count;

	if ( cache [ len ] != 0 )
		return cache [ len ];

	int res;
	if ( len == 1 )
		res = array [ 0 ];
	else
		res = array [ 0 ] > maximum ( array + 1, len - 1 ) ? array [ 0 ] : maximum ( array + 1, len - 1 );

	cache [ len ] = res;
	return res;
}

int main ( ) {
	int array[] = {
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
//		12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1
	};
	int max = maximum ( array, sizeof ( array ) / sizeof ( * array ) );
	printf ( "max = %d\ncount = %u\n", max, g_count );

	return 0;
}
