#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct TElement {
	int m_Value;
	struct TElement * m_Next;
} TELEMENT;

typedef struct TQueue {
	TELEMENT * m_Head;
	TELEMENT * m_Tail;
} TQUEUE;

TQUEUE * QueueInit ( ) {
	TQUEUE * res = ( TQUEUE * ) malloc ( sizeof ( TQUEUE ) );
	res->m_Head = NULL;
	res->m_Tail = NULL;
	return res;
}

int QueueIsEmpty ( TQUEUE * q ) {
	return q -> m_Head == NULL;
}

int QueuePut ( TQUEUE * q, int x ) {
	TELEMENT * e = ( TELEMENT * ) malloc ( sizeof ( TELEMENT ) );
	e -> m_Value = x;
	e -> m_Next = NULL;
	if ( q -> m_Head == NULL ) {
		q -> m_Head = q -> m_Tail = e;
	} else {
		q -> m_Tail -> m_Next = e;
		q -> m_Tail = e;
	}
	return 1;
}

int QueueGet ( TQUEUE * q, int * x ) {
	if ( q -> m_Head == NULL )
		return 0;

	TELEMENT * tmp = q -> m_Head;
	* x = tmp -> m_Value;

	q -> m_Head = q -> m_Head -> m_Next;
	free ( tmp );
	if ( q -> m_Head == NULL )
		q -> m_Tail = NULL;

	return 1;
}

void QueueDone ( TQUEUE * q ) {
	while ( ! QueueIsEmpty ( q ) ) {
		int x;
		QueueGet ( q, & x );
	}
	free ( q );
}

int main ( ) {
	TQUEUE * q = QueueInit ( );

	QueuePut ( q, 1 );
	QueuePut ( q, 2 );
	QueuePut ( q, 3 );

	int x;
	QueueGet ( q, & x );
	assert ( x == 1 );
	QueueGet ( q, & x );
	assert ( x == 2 );
	QueueGet ( q, & x );
	assert ( x == 3 );

	assert ( QueueGet ( q, & x ) == 0 );

	for ( int i = 0; i < 10; i ++ ) {
		assert ( QueuePut ( q, i ) == 1 );
	}

	for ( int i = 0; i < 10; i ++ ) {
		int x;
		assert ( QueueGet ( q, & x ) == 1 );
		assert ( x == i );
	}

	assert ( QueueGet ( q, & x ) == 0 );

	for ( int i = 0; i < 10; i ++ ) {
		assert ( QueuePut ( q, i ) == 1 );
	}

	assert ( QueueIsEmpty ( q ) == 0 );

	while ( ! QueueIsEmpty ( q ) ) {
		int x;
		QueueGet ( q, & x );
	}

	assert ( QueueIsEmpty ( q ) == 1 );

	QueuePut ( q, 1 );

	QueueDone ( q );

	return 0;
}

