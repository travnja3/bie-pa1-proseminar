#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct TQueue {
 int m_Len;
 int m_Max;
 int m_Rd;
 int m_Wr;
 int * m_Data;
} TQUEUE;

TQUEUE * QueueInit ( int max ) {
	TQUEUE * q = ( TQUEUE * ) malloc ( sizeof ( TQUEUE ) );
	q -> m_Len = 0;
	q -> m_Max = max;
	q -> m_Rd = 0;
	q -> m_Wr = 0;
	q -> m_Data = ( int * ) malloc ( sizeof ( int ) * max );
	return q;
}

void QueueDone ( TQUEUE * q ) {
	free ( q -> m_Data );
	free ( q );
}

int QueuePut ( TQUEUE * q, int x ) {
	if ( q -> m_Len < q -> m_Max ) {
		q -> m_Data [ q -> m_Wr ] = x;
		q -> m_Wr = ( q -> m_Wr + 1 ) % q -> m_Max;
		++ q -> m_Len;
		return 1;
	} else {
		// handle not enough space in the queue
		return 0;
	}
}

int QueueGet ( TQUEUE * q, int * x ) {
	if ( q -> m_Len > 0 ) {
		* x = q -> m_Data [ q -> m_Rd ];
		q -> m_Rd = ( q -> m_Rd + 1 ) % q -> m_Max;
		-- q -> m_Len;
		return 1;
	} else {
		// handle no value in the queue
		return 0;
	}
}

int QueueIsEmpty ( TQUEUE * q ) {
	return q -> m_Len == 0;
}

int main ( ) {
	TQUEUE * q = QueueInit ( 10 );

	QueuePut ( q, 1 );
	QueuePut ( q, 2 );
	QueuePut ( q, 3 );

	int x;
	QueueGet ( q, & x );
	assert ( x == 1 );
	QueueGet ( q, & x );
	assert ( x == 2 );
	QueueGet ( q, & x );
	assert ( x == 3 );

	assert ( QueueGet ( q, & x ) == 0 );

	for ( int i = 0; i < 10; i ++ ) {
		assert ( QueuePut ( q, i ) == 1 );
	}

	for ( int i = 0; i < 10; i ++ ) {
		int x;
		assert ( QueueGet ( q, & x ) == 1 );
		assert ( x == i );
	}

	assert ( QueueGet ( q, & x ) == 0 );

	for ( int i = 0; i < 10; i ++ ) {
		assert ( QueuePut ( q, i ) == 1 );
	}

	assert ( QueueIsEmpty ( q ) == 0 );

	while ( ! QueueIsEmpty ( q ) ) {
		int x;
		QueueGet ( q, & x );
	}

	assert ( QueueIsEmpty ( q ) == 1 );

	QueueDone ( q );

	return 0;
}

