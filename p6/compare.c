#include <stdio.h>
#include <stdlib.h>

#define ARRAY_MAX 100

void printArray ( int array [], int size ) {
	for ( int i = 0; i < size; ++ i ) {
		printf ( "%d ", array [ i ] );
	}
	printf ( "\n" );
}

int intcmp ( int * a, int * b ) {
	return ( * b < * a ) - ( * a < * b );
}

int readArray ( int array [], int * size ) {
	printf ( "Enter number of elements:\n" );
	if ( scanf ( "%d", size ) != 1 || * size < 1 || * size > ARRAY_MAX ) {
		printf ( "Invalid input.\n" );
		return 0;
	}

	for ( int i = 0; i < * size; ++ i ) {
		if ( scanf ( "%d", & array [ i ] ) != 1 ) {
			printf ( "Invalid input.\n" );
			return 0;
		}
	}

	return 1;
}

int searchArray ( int array [], int size, int value ) {
	for ( int i = 0; i < size; ++ i )
		if ( array [ i ] == value )
			return 1;
	return 0;
}

int compareArrays ( int array1 [], int size1, int array2 [], int size2 ) {
	if ( size1 != size2 )
		return 0;

	for ( int i = 0; i < size1; ++ i ) {
		if ( array1 [ i ] != array2 [ i ] )
			return 0;
	}
	return 1;
}

int main ( ) {
	int array1_size;
	int array2_size;
	int array1 [ ARRAY_MAX ];
	int array2 [ ARRAY_MAX ];

	if ( ! readArray ( array1, & array1_size ) || ! readArray ( array2, & array2_size ) ) {
		printf ( "Invalid input.\n" );
		return 1;
	}

	qsort ( array1, array1_size, sizeof ( array1 [ 0 ] ), (int(*)(const void *, const void *)) intcmp );
	printArray ( array1, array1_size );
	qsort ( array2, array2_size, sizeof ( array2 [ 0 ] ), (int(*)(const void *, const void *)) intcmp );
	printArray ( array2, array2_size );

	if ( compareArrays ( array1, array1_size, array2, array2_size ) ) {
		printf ( "Same content.\n" );
	} else {
		printf ( "Different content.\n" );
	}

	return 0;
}
