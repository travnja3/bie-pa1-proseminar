#include <stdio.h>
#include <stdlib.h>

#define ARRAY_MAX 100

void printArray ( int array [], int size ) {
	for ( int i = 0; i < size; ++ i ) {
		printf ( "%d ", array [ i ] );
	}
	printf ( "\n" );
}

/*int searchArray ( int array [], int size, int value ) {
	for ( int i = 0; i < size; ++ i )
		if ( array [ i ] == value )
			return 1;
	return 0;
}*/

int binsearchArray ( int array [], int array_size, int value ) {
	int lo = 0;
	int hi = array_size;
	while ( lo != hi ) {
		int mi = ( lo + hi ) / 2;
		if ( array [ mi ] == value )
			return 1;
		if ( array [ mi ] > value ) {
			hi = mi;
		} else { // array [ mi ] < value
			lo = mi + 1;
		}
	}
	return 0;
}

int intcmp ( int * a, int * b ) {
	return ( * b < * a ) - ( * a < * b );
}

int main ( ) {
	int array_size;
	int array [ ARRAY_MAX ];

	printf ( "Enter number of elements:\n" );
	if ( scanf ( "%d", & array_size ) != 1 || array_size < 1 || array_size > ARRAY_MAX ) {
		printf ( "Invalid input.\n" );
		return 1;
	}

	for ( int i = 0; i < array_size; ++ i ) {
		if ( scanf ( "%d", & array [ i ] ) != 1 ) {
			printf ( "Invalid input.\n" );
			return 2;
		}
	}

	printArray ( array, array_size );

	qsort ( array, array_size, sizeof ( array [ 0 ] ), (int(*)(const void *, const void *)) intcmp );

	printArray ( array, array_size );

	int query;
	while ( scanf ( "%d", & query ) == 1 ) {
		if ( binsearchArray ( array, array_size, query ) ) {
			printf ( "%d is in the array.\n", query );
		} else {
			printf ( "%d is not in the array.\n", query );
		}
	}

	if ( ! feof ( stdin ) ) {
		printf ( "Invalid input.\n" );
		return 3;
	}

	return 0;
}
