#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void removeLF ( char * line ) {
	size_t len = strlen ( line );

	if ( len > 0 && line [ len - 1 ] == '\n' )
		line [ len - 1 ] = '\0';
}

int occurrenceAtBegining ( char * needle, char * string ) {
	while ( * needle != '\0' ) {
		if ( * needle != * string ) {
			return 0;
		}
		++ needle;
		++ string;
	}

	return 1;
}

void search ( char * needle, char * haystack ) {
	int found = 0;
	for ( char * tmp = haystack; * tmp; ++ tmp ) {
		if ( occurrenceAtBegining ( needle, tmp ) ) {
			if ( ! found ) {
				printf ( "Pattern found - positions:" );
				found = 1;
			}
			printf ( " %ld", tmp - haystack );
		}
	}
	if ( ! found ) {
		printf ( "Pattern not found\n" );
	} else {
		printf ( "\n" );
	}
}

int main ( ) {
	printf ( "Enter needle:\n" );
	char needle [100];
	if ( ! fgets ( needle, 100, stdin ) ) {
		printf ( "Invalid input.\n" );
		return 0;
	}

	removeLF ( needle );

	char *haystack = NULL;
	size_t len = 0;

	printf ( "Enter haystacks:\n" );
	while ( getline ( & haystack, & len, stdin ) != EOF ) {
		removeLF ( haystack );
		search ( needle, haystack );
		free ( haystack );
		haystack = NULL;
		len = 0;
	}

	free ( haystack );

	return 0;
}
