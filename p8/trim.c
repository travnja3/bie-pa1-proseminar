#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void removeLF ( char * line ) {
	size_t len = strlen ( line );

	if ( len > 0 && line [ len - 1 ] == '\n' )
		line [ len - 1 ] = '\0';
}

void removeSpaces ( char * line ) {
	int lastWhiteSpace = 1;

	char * src = line;
	char * dst = line;
	while ( * src != '\0' ) {
		if ( * src == ' ' ) {
			if ( ! lastWhiteSpace ) {
				* dst = * src;
				++ dst;
				lastWhiteSpace = 1;
			}
		} else {
			* dst = * src;
			++ dst;
			lastWhiteSpace = false;
		}
		++ src;
	}
	if ( dst != line && dst [ -1 ] == ' ' )
		-- dst;
	* dst = '\0';
}

int main ( ) {
	printf ( "Enter text:\n" );
	/*char line [100];
	while ( fgets ( line, 100, stdin ) ) {
		printf ( ">%s<\n", line );
	}*/

	char *line = NULL;
	size_t len = 0;

	while ( getline ( & line, & len, stdin ) != EOF ) {
		removeLF ( line );
		removeSpaces ( line );
		printf ( ">%s<\n", line );
		free ( line );
		line = NULL;
		len = 0;
	}

	free ( line );

	return 0;
}
