#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main ( ) {
	int max, primes = 0;
	printf ( "Enter maximum (inlcusive):\n" );

	if ( scanf ( "%d", & max ) != 1 || max <= 1 ) {
		printf ( "Invalid input.\n" );
		return 1;
	}

	int arraySize = ( ( max + 1 ) + 7 ) / 8;

	char * array = ( char * ) malloc ( sizeof ( * array ) * arraySize );
	// 0 not a prime
	// anything else is a prime or prime candidate

	memset ( array, 0xff, sizeof ( * array) * arraySize );
	array [ 0 ] &= ~3; // 0 and are not primes

	int maxSqrt = sqrt ( max );

	for ( int i = 2; i <= max; ++ i ) {
		if ( ( array [ i >> 3 ] & ( 1 << ( i & 7 ) ) ) == 0 )
			continue;

		// i represents a prime
		if ( i > 2 )
			printf ( " " );
		printf ( "%d", i );
		++ primes;

		if ( i <= maxSqrt )
			for ( int j = i * i; j <= max; j += i ) {
				array [ j >> 3 ] &= ~ ( 1 << ( j & 7 ) );
			}
	}

	printf ( "\nThe total number of primes identified is %d\n", primes );

	free ( array );

	return 0;
}
