#include <stdio.h>
#include <stdlib.h>

int main ( ) {
	int x;

	int arrayMaxSize = 0;
	int * array = NULL;
	int arraySize = 0;

	while ( scanf ( "%d", & x ) == 1 ) {
		if ( arraySize >= arrayMaxSize ) {
			arrayMaxSize += arrayMaxSize > 10 ? arrayMaxSize * .5 : 2;
			array = ( int * ) realloc ( array, arrayMaxSize * sizeof ( * array ) );
		}
		array [ arraySize ++ ] = x;
	}

	if ( ! feof ( stdin ) ) {
		printf ( "Invalid input.\n" );
		return 1;
	}

	for ( int i = 0; i < arraySize; ++ i ) {
		if ( i != 0 )
			printf ( " " );
		printf ( "%d", array [ i ] );
	}

	free ( array );

	return 0;
}
