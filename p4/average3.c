#include <stdio.h>

int main ( ) {
	int n = 0;
	int max, min;
	int sum = 0;
	printf ( "Give a sequence.\n" );

	// read the rest of input, the actual sequence
	int x;
	while ( scanf ( "%d", &x ) == 1 ) {
		if ( n == 0 || max < x )
			max = x;
		if ( n == 0 || min > x )
			min = x;

		sum += x;

		++ n;
	}

	if ( ! feof ( stdin ) ) {
		printf ( "Invalid input.\n" );
		return 1;
	}

	if ( n == 0 ) {
		printf ( "Invalid input.\n" );
		return 2;
	}

	printf ( "Min = %d, max = %d, average = %g\n", min, max, (double) sum / n );

	return 0;
}
