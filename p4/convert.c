#include <stdio.h>

int main ( ) {
	int number;
	printf ( "Give a number to convert:\n" );
	if ( scanf ( "%d", & number ) != 1 || number < 0 ) {
		printf ( "Invalid input.\n" );
		return 1;
	}

	// i = 1 0 0 0 0 ... 31 -zeros 0 0 0 0
	int printZeros = 0;
	for ( unsigned i = 1 << ( sizeof ( int ) * 8 - 1 ); i > 0; i >>= 1 ) {
		if ( number & i )
			printZeros = 1;
		if ( number & i || printZeros )
			printf ( "%d", ! ! ( number & i ) );
	}
	printf ( "\n" );
	return 0;
}
