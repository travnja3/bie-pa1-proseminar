#include <stdio.h>

int main ( ) {
	// read base, read exponent
	double a;
	long long e;

	printf ( "Give the base and exponent.\n" );
	if ( scanf ( "%lf %lld", &a, &e ) != 2 || e < 0 ) {
		printf ( "Invalid input.\n" );
		return 1;
	}

	// compute
	/*double res = 1;
	for ( int i = 0; i < e; ++ i ) {
		res *= a;
	}*/

	double res = 1;
	double tmp = a;
	for ( long long i = 1; i; i <<= 1 ) {
		if ( e & i )
			res *= tmp;
		tmp *= tmp;
	}

	// print result
	printf ( "%f^%lld = %f\n", a, e, res );
	return 0;
}
