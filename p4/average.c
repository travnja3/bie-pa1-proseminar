#include <stdio.h>

int main ( ) {
	int n;
	printf ( "Give the length of a sequence:\n" );
	if ( scanf ( "%d", &n ) != 1 || n <= 0 ) {
		printf ( "Invalid input.\n" );
		return 1;
	}

	int max, min;
	int sum = 0;
	printf ( "Give the sequence itself.\n" );
	// read the rest of input, the actual sequence
	for ( int i = 0; i < n; ++ i ) {
		int x;
		if ( scanf ( "%d", &x ) != 1 ) {
			printf ( "Invalid input.\n" );
			return 2;
		}

		if ( i == 0 || max < x )
			max = x;
		if ( i == 0 || min > x )
			min = x;

		sum += x;
	}

	printf ( "Min = %d, max = %d, average = %g\n", min, max, (double) sum / n );

	return 0;
}
