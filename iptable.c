#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Node {
	struct Node * left, * right;
	char * owner;
};

struct Node * newEmptyNode ( ) {
	struct Node * res = ( struct Node * ) malloc ( sizeof ( struct Node ) );
	res -> left = res -> right = NULL;
	res -> owner = NULL;
	return res;
}

int isLeaf ( struct Node * node ) {
	return node -> left == NULL && node -> right == NULL;
}

int allocateBlock ( struct Node * node, unsigned ip, int mask, char * owner ) {
	while ( 1 ) {
		if ( node->owner )
			return 0;

		if ( mask == 0 ) {
			if ( ! isLeaf ( node ) )
				return 0;
			if ( node -> owner )
				return 0;

			node -> owner = strdup ( owner );
			return 1;
		}

		if ( isLeaf ( node ) ) {
			node -> left = newEmptyNode ( );
			node -> right = newEmptyNode ( );
		}

		if ( ip & 0x80000000 )
			node = node -> right;
		else
			node = node -> left;

		ip <<= 1;
		-- mask;
	}
}

int freeBlock ( struct Node * node, unsigned ip, int mask ) {
	if ( mask == 0 ) {
		if ( ! node -> owner )
			return 0;
		free ( node -> owner );
		node -> owner = NULL;
		return 1;
	}

	if ( isLeaf ( node ) )
		return 0;

	if ( ! freeBlock ( ip & 0x80000000 ? node -> right : node -> left, ip << 1, -- mask ) )
		return 0;

	if ( isLeaf ( node -> left ) && node -> left -> owner == NULL && isLeaf ( node->right ) && node -> right -> owner == NULL ) {
		free ( node -> left );
		free ( node -> right );
		node -> left = NULL;
		node -> right = NULL;
		return 1;
	} else {
		return 0;
	}
}

void printIp ( unsigned ip, int mask ) {
	unsigned x1, x2, x3, x4;
	x1 = ip >> 24;
	x2 = ( ip >> 16) & 0xFF;
	x3 = ( ip >> 8) & 0xFF;
	x4 = ( ip ) & 0xFF;

	printf ( "%u.%u.%u.%u/%d", x1, x2, x3, x4, mask );
}

void printNode ( unsigned ip, int mask, char * owner ) {
	printIp ( ip, mask );
	printf ( " %s\n", owner);
}

void printDatabase ( struct Node * node, unsigned ip, int mask ) {
	if ( node == NULL )
		return;

	if ( isLeaf ( node ) && node -> owner ) {
		printNode ( ip, mask, node->owner );
	}

	printDatabase ( node -> left, ip, mask + 1 );
	printDatabase ( node -> right, ip | 1 << ( 31 - mask ), mask + 1 );
}

void printDatabase ( struct Node * root ) {
	printDatabase ( root, 0, 0 );
}

int parseIp ( char * input, unsigned * ip, int * mask ) {
	unsigned x1, x2, x3, x4;
	if ( sscanf ( input, "%u.%u.%u.%u/%d", &x1, &x2, &x3, &x4, mask ) != 5 )
		return 0;

	// construct the ip address
	if ( x1 >= 256 || x2 >= 256 || x3 >= 256 || x4 >= 256 || *mask <= 0 || *mask > 30 )
		return 0;
	*ip = x1 << 24 | x2 << 16 | x3 << 8 | x4;
	if ( *ip & ~ ( (int) 0x80000000 >> (* mask - 1) ) ) // if mask == 2 -> 0x80000000 >> 1 -> x = 0xB0000000 -> ~ x = 0x3FFFFFFF
		return 0;
	return 1;
}

void printMenu ( ) {
	printf ( "1 - alloc\n2 - free\n3 - print\n" );
}

void freeDatabase ( struct Node * node ) {
	if ( node == NULL )
		return;
	freeDatabase ( node -> left );
	freeDatabase ( node -> right );
	free ( node -> owner );
	free ( node );
}

int main ( ) {
	int exit = 0;
	struct Node * root = newEmptyNode ( );

	while ( ! exit ) {
		int choice, res;
		char input[100];
		char owner[100];
		unsigned ip;
		int mask;

		printMenu ( );
		res = scanf ( "%d", & choice );
		if ( feof ( stdin ) ) {
			break;
		}
		if ( res != 1 ) {
			printf ( "Invalid input.\n" );
			break;
		}
		switch ( choice ) {
		case 1:
			scanf ( "%99s", input );
			if ( ! parseIp ( input, & ip, & mask ) ) {
				printf ( "Invalid input.\n" );
				exit = 1;
				break;
			}
			scanf ( "%99s", owner );
			printf ( "Allocating ip %d, mask %d, owner %s\n", ip, mask, owner);
			if ( ! allocateBlock ( root, ip, mask, owner ) ) {
				printf ( "Invalid allocation.\n" );
			}
			break;
		case 2:
			fgets ( input, 100, stdin );
			if ( ! parseIp ( input, & ip, & mask ) ) {
				printf ( "Invalid input.\n" );
				exit = 1;
				break;
			}
			freeBlock ( root, ip, mask );
			break;
		case 3:
			printDatabase ( root );
			break;
		default:
			exit = 1;
		}
	}

	freeDatabase ( root );

	return 0;
}
