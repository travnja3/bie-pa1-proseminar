#include <stdio.h>
#include <math.h>
#include <float.h>

int main ( ) {
	printf ( "Enter triangle sides:\n" );

	double a, b, c;

	if ( scanf ( "%lf %lf %lf", &a, &b, &c ) != 3 || a <= 0 || b <= 0 || c <= 0 ) {
		printf ( "Invalid input.\n" );
		return 0;
	}

	if ( a < b ) {
		double tmp = a;
		a = b;
		b = tmp;
	}

	if ( a < c ) {
		double tmp = a;
		a = c;
		c = tmp;
	}

	// a is hypotenus
	// b, c are sides

	if ( c + b <= a ) {
		printf ( "Triange does not exist.\n" );
		return 0;
	}

//	printf ( "%.100f\n", fabs ( a * a - b * b - c * c ) );
//	printf ( "%.100f\n", 1e-12 * a * a );
	if ( fabs ( a * a - b * b - c * c ) < DBL_EPSILON * 1000 * a * a ) {
		printf ( "Right angled.\n" );
	} else {
		printf ( "Not right angled.\n" );
	}

	return 0;
}
