#include <stdio.h>

double toImperial ( int yd, int ft, int in ) {
	return (3 * 12 * yd + 12 * ft + in ) * 2.54;
}

void toMetric ( int cm, int * yd, int * ft, double * in ) {
	* yd = cm / 2.54 / 12 / 3;
	cm -= * yd * 3 * 12 * 2.54;

	* ft = cm / 2.54 / 12;
	cm -= * ft * 12 * 2.54;

	* in = cm / 2.54;
}

int main ( ) {
	int cm, yd, ft, in;
	char newline;
	int res;

	if ( ( res = scanf ( "%d cm%c", & cm, & newline ) ) == 2 && newline == '\n') {
		double in_d;
		toMetric ( cm, & yd, & ft, & in_d );
		printf ( "%d cm to metric %d yd %d ft %f in\n", cm, yd, ft, in_d );
	} else if ( res == 1 && scanf ( " yd %d ft %d in%c", & ft, & in, & newline ) == 3 && newline == '\n' ) {
		yd = cm;
		printf ( "%d yd %d ft %d in to metric %f cm\n", yd, ft, in, toImperial ( yd, ft, in )  );
	} else { // invalid
		printf ( "Invalid input.\n" );
		return 1;
	}
	return 0;
}
