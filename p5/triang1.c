#include <stdio.h>

unsigned fact ( unsigned n ) {
	unsigned res = 1;
	while ( n > 1 ) {
		res *= n --;
	}
	return res;
}

unsigned comb ( unsigned n, unsigned k ) {
	return fact ( n ) / fact ( k ) / fact ( n - k );
}

void triangleRow ( unsigned rowno ) {
	for ( unsigned colno = 0; colno <= rowno; ++ colno ) {
		printf ( "%d ", comb ( rowno, colno ) );
	}
}

void pascalsTriangle ( unsigned rows ) {
	-- rows;

	for ( unsigned rowno = 0; rowno <= rows; ++ rowno ) {
		triangleRow ( rowno );
		printf ( "\n" );
	}
}

int main ( ) {
	unsigned rows;
	printf("Number of rows:\n");
	if ( scanf ( "%u", & rows ) != 1 || rows == 0 ) {
		printf ( "Invalid input.\n" );
		return 1;
	}

	pascalsTriangle ( rows );

	return 0;
}
