#include <stdio.h>
#include <limits.h>

unsigned fact ( unsigned n ) {
	unsigned res = 1;
	while ( n > 1 ) {
		res *= n --;
	}
	return res;
}

unsigned comb ( unsigned n, unsigned k ) {
	if ( k > n / 2 )
		k = n - k;

	unsigned res = 1;
	for ( unsigned i = 1; i <= k; ++ i ) {
		if ( res > UINT_MAX / ( n - i + 1 ) )
			return 0;

		res = res * ( n - i + 1 ) / i;
	}
	return res;
}

int triangleRow ( unsigned rowno ) {
	for ( unsigned colno = 0; colno <= rowno; ++ colno ) {
		unsigned value = comb ( rowno, colno );
		if ( value == 0 )
			return 0;
		printf ( "%d ", value );
	}
	return 1;
}

int pascalsTriangle ( unsigned rows ) {
	-- rows;

	for ( unsigned rowno = 0; rowno <= rows; ++ rowno ) {
		unsigned flag = triangleRow ( rowno );
		if ( flag == 0 )
			return 0;
		printf ( "\n" );
	}
	return 1;
}

int main ( ) {
	int rows;
	printf("Number of rows:\n");
	if ( scanf ( "%d", & rows ) != 1 || rows < 1 ) {
		printf ( "Invalid input.\n" );
		return 1;
	}

	if ( pascalsTriangle ( rows ) == 0 ) {
		printf ( "\nOverflow!!\n" );
	}

	return 0;
}
